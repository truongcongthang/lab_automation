# Command to install update chromedriver on ubuntu

sudo apt-get update
sudo apt-get --only-upgrade install google-chrome-stable -y
rm -rf chromedriver_linux64.zip

# Note - change the link here
wget -N https://chromedriver.storage.googleapis.com/91.0.4472.19/chromedriver_linux64.zip
unzip chromedriver_linux64.zip
chmod +x chromedriver
sudo rm /usr/bin/chromedriver
sudo rm /usr/local/bin/chromedriver
sudo mv -f chromedriver /usr/local/share/chromedriver
sudo ln -s /usr/local/share/chromedriver /usr/local/bin/chromedriver
sudo ln -s /usr/local/share/chromedriver /usr/bin/chromedriver
rm -rf chromedriver_linux64.zip
sudo apt autoremove -y