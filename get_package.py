import argparse
import zipfile
import os
import requests

LATEST_VERSION = 'https://chromedriver.storage.googleapis.com/LATEST_RELEASE'
# This is beta-production
# SEE LINK: https://selenium-release.storage.googleapis.com/index.html
SELENIUM_GRID_V4 = 'https://selenium-release.storage.googleapis.com/4.0-beta-3/selenium-server-4.0.0-beta-3.jar'
# This is not supported anymore
SELENIUM_GRID_V3 = 'https://selenium-release.storage.googleapis.com/3.141/selenium-server-standalone-3.141.59.jar'


def download_file(url):
    # Download file
    response = requests.get(url, allow_redirects=True)
    print(response.headers.get('content-type'))
    filename = url.rsplit('/', 1)[1]
    open(filename, 'wb').write(response.content)
    # Unzip file
    if 'zip' in filename:
        with zipfile.ZipFile(filename) as z:
            z.extractall('.')
        # remove the zip_file
        os.remove(filename)

    # rename
    # if 'selenium-server' in filename:
    #     os.rename(filename, 'selenium-server.jar')


def get_latest_chromedriver_version(os_type):
    response = requests.get('https://chromedriver.storage.googleapis.com/LATEST_RELEASE')
    version = response.text
    url = f'https://chromedriver.storage.googleapis.com/{version}/chromedriver_{os_type}.zip'

    return version, url


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Test Execution handling')
    parser.add_argument('--os', help='ex: --os=win32 or --os=linux64', default="linux64", required=False)

    parameters = parser.parse_args()
    os_type = parameters.os

    latest_chromedriver_url = get_latest_chromedriver_version(os_type)[1]
    download_file(url=latest_chromedriver_url)
    download_file(url=SELENIUM_GRID_V3)
